#include "DatabaseAccess.h"

std::vector<int> ids;
std::list<Album> albums;
std::list<Picture> pictures;
std::list<User> users;

DatabaseAccess::DatabaseAccess()
{
	this->open();
}

DatabaseAccess::~DatabaseAccess()
{
	this->close();
}

bool DatabaseAccess::open()
{
	int file_exist = _access(NAME, 0);
	int res = sqlite3_open(NAME, &this->_db);

	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	if (file_exist != 0)
	{
		// init database

		std::string sqlCreateStatements[TABLES_CNT] = {	"CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL);",
											"CREATE TABLE ALBUMS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, CREATION_DATE DATE NOT NULL,USER_ID INTEGER NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS(ID));",
											"CREATE TABLE PICTURES (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE DATE NOT NULL, ALBUM_ID INTEGER NOT NULL, FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID));",
											"CREATE TABLE TAGS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,PICTURE_ID INTEGER NOT NULL, USER_ID INTEGER NOT NULL, FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID),  FOREIGN KEY (USER_ID) REFERENCES USERS(ID));" };
		
		for (int i = 0; i < TABLES_CNT; i++)
		{
			try
			{
				this->runSqlStatement(sqlCreateStatements[i], nullptr, nullptr, "could not init database");
			}
			catch (std::exception& e)
			{
				std::remove(NAME);
				return false;
				std::cerr << e.what() << std::endl;
			}
			
		}
	}
	else
	{
		//loading data
		this->getAlbums();
		this->getUsers();
	}
	
	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}

void DatabaseAccess::clear()
{
	_m_albums.clear();
	_m_users.clear();
	this->close();
	if (remove(NAME) != 0) std::cerr << "Error deleting db" << std::endl;
	else if (!this->open()) std::cerr << "Error opening db" << std::endl;
}

void DatabaseAccess::runSqlStatement(std::string sql_statement, int (*callback)(void* data, int argc, char** argv, char** azColName), void* data_output, std::string errOutput)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->_db, sql_statement.c_str(), callback, data_output, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cerr << "Failed to run " << sql_statement << " in DB" << std::endl << errMessage << std::endl;
		throw MyException(errOutput);
	}
}

void DatabaseAccess::closeAlbum(Album& album)
{

}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	for (auto iter = _m_albums.begin(); iter != _m_albums.end(); iter++) {
		if (iter->getName() == albumName && iter->getOwnerId() == userId) {
			pictures.clear();
			pictures = iter->getPictures();
			for (auto pic = pictures.begin(); pic != pictures.end(); ++pic)
			{
				//deleting from tags and pictures
				this->runSqlStatement("DELETE FROM TAGS WHERE PICTURE_ID = " + std::to_string(pic->getId()) + "; ", nullptr, nullptr, "could not delete album from database");
				this->runSqlStatement("DELETE FROM PICTURES WHERE PICTURE_ID = " + std::to_string(pic->getId()) + "; ", nullptr, nullptr, "could not delete album from database");
			}
			iter = _m_albums.erase(iter);
			break;
		}
	}
	//deleting from albums
	this->runSqlStatement("DELETE FROM ALBUMS WHERE NAME = \"" + albumName + "\" AND USER_ID =" + std::to_string(userId) + "; ", nullptr, nullptr, "could not delete album from database");
}

void DatabaseAccess::createUser(User& user)
{
	_m_users.push_back(user);
	this->runSqlStatement("INSERT INTO USERS(NAME) VALUES(\"" + user.getName() + "\");", nullptr, nullptr, "could not add user to database");
}

void DatabaseAccess::deleteUser(const User& user)
{
	std::string errorMsg = "could not delete user data from database";
	
	this->runSqlStatement("BEGIN;", nullptr, nullptr, errorMsg);

	this->cleanUserData(user);

	if (doesUserExists(user.getId())) {

		for (auto iter = _m_users.begin(); iter != _m_users.end(); ++iter)
		{
			if (*iter == user)
			{
				iter = _m_users.erase(iter);
				break;
			}
		}
	}

	this->runSqlStatement("DELETE FROM USERS WHERE NAME = \"" + user.getName() + "\"; ", nullptr, nullptr, errorMsg);

	this->runSqlStatement("COMMIT;", nullptr, nullptr, errorMsg);
}

void DatabaseAccess::cleanUserData(const User& user)
{
	std::string errorMsg = "could not delete user data from database";

	this->runSqlStatement("DELETE FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + "; ", nullptr, nullptr, errorMsg);

	if (this->doesUserExists(user.getId()))
	{
		const std::list <Album> user_al = this->getAlbumsOfUser(user);
		for (auto it = user_al.begin(); it != user_al.end(); ++it)
		{
			for (auto itPic = it->getPictures().begin(); itPic != it->getPictures().end(); ++itPic)
			{
				this->runSqlStatement("DELETE FROM PICTURES WHERE ID = " + std::to_string(itPic->getId()) + "; ", nullptr, nullptr, errorMsg);
			}

			this->deleteAlbum(it->getName(), user.getId());
		}
		for (auto it = _m_albums.begin(); it != _m_albums.end(); ++it)
		{
			it->untagUserInAlbum(user.getId());
		}
	}

	this->runSqlStatement("DELETE FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + "; ", nullptr, nullptr, errorMsg);
}

auto DatabaseAccess::getAlbumIfExists(const std::string& albumName)
{
	auto result = std::find_if(std::begin(_m_albums), std::end(_m_albums), [&](auto& album) { return album.getName() == albumName; });

	if (result == std::end(_m_albums)) 
	{
		throw ItemNotFoundException("Album not exists: ", albumName);
	}
	return result;

}

auto DatabaseAccess::getPicture(const int pictureId)
{
	for (const auto& album : this->_m_albums) {
		for (const auto& picture : album.getPictures()) {
			if (picture.getId() == pictureId) return picture;
		}
	}
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	auto result = getAlbumIfExists(albumName);

	this->runSqlStatement("INSERT INTO TAGS (PICTURE_ID, USER_ID) VALUES( " + std::to_string((*result).getPicture(pictureName).getId()) + ",  " + std::to_string(userId) + "); ", nullptr, nullptr, "could not tag user from picture in data base");

	(*result).tagUserInPicture(userId, pictureName);
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	auto result = getAlbumIfExists(albumName);
	
	this->runSqlStatement("DELETE FROM TAGS WHERE PICTURE_ID = " + std::to_string((*result).getPicture(pictureName).getId()) + "; ", nullptr, nullptr, "could not untag user from picture in data base");

	(*result).untagUserInPicture(userId, pictureName);
}

int DatabaseAccess::callback_pictures(void* data, int argc, char** argv, char** azColName)
{
	pictures.push_back(Picture(std::atoi(argv[0]), argv[1], argv[2], argv[3]));
	return 0;
}

int DatabaseAccess::callback_albums(void* data, int argc, char** argv, char** azColName)
{
	albums.push_back(Album(std::atoi(argv[3]), std::string(argv[1]), std::string(argv[2])));
	return 0;
}

void DatabaseAccess::setDataOfAlbums(std::list<Album>& albums)
{
	for (std::list<Album>::iterator album =albums.begin() ; album!=albums.end(); ++album)
	{
		pictures.clear();
		this->runSqlStatement("SELECT * FROM PICTURES WHERE ALBUM_ID=" + std::to_string(this->getAlbumId(album->getName())) + ';', DatabaseAccess::callback_pictures, nullptr, "could not get pictures");
		for (std::list<Picture>::iterator picture = pictures.begin(); picture != pictures.end(); ++picture)
		{
			//getting tags of picture
			ids.clear();
			this->runSqlStatement("SELECT USER_ID FROM TAGS WHERE PICTURE_ID=" + std::to_string(picture->getId()) + ';', DatabaseAccess::callback_get_ids_or_count, nullptr, "could not get tags");
			for (int i = 0; i < ids.size(); i++)
			{
				picture->tagUser(ids[i]);
			}

			//adding picture to album
			album->addPicture(*picture);
		}
	}
}

const std::list<Album> DatabaseAccess::getAlbums()
{
	albums.clear();
	this->runSqlStatement("SELECT * FROM ALBUMS;", DatabaseAccess::callback_albums, nullptr, "could not get albums");
	this->setDataOfAlbums(albums);
	this->_m_albums.assign(albums.begin(), albums.end());
	return albums;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	albums.clear();
	this->runSqlStatement("SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + ";", DatabaseAccess::callback_albums, nullptr, "could not get albums");
	this->setDataOfAlbums(albums);
	return albums;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	//adding to ALBUMS
	this->_m_albums.push_back(album);
	this->runSqlStatement("INSERT INTO ALBUMS (NAME, USER_ID, CREATION_DATE) VALUES(\"" + album.getName() + "\", " + std::to_string(album.getOwnerId()) + ", \"" + album.getCreationDate() + "\");", nullptr, nullptr, "Could not add album to albums");
	//adding to PICTURES
	for (const Picture& picture : album.getPictures())
	{
		this->addPictureToAlbumByName(album.getName(), picture);
	}
}

int DatabaseAccess::getAlbumId(const std::string& albumName)
{
	std::vector<int> id;
	ids.clear();
	this->runSqlStatement("SELECT ID FROM ALBUMS WHERE NAME =\"" + albumName + "\"; ", DatabaseAccess::callback_get_ids_or_count, &id, "could not find album id");
	if (!id.empty())return id[0];
	else throw MyException("error getting album id");
}

bool DatabaseAccess::doesUserExists(int userId)
{
	int res = 0;
	this->runSqlStatement("SELECT * FROM USERS WHERE ID= " + std::to_string(userId) + "; ", DatabaseAccess::callback_check_if_got_results, &res, "could not check if user exists");
	return res == 1;
}

bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	int res = 0;
	this->runSqlStatement("SELECT * FROM ALBUMS WHERE NAME =\"" + albumName + "\" AND USER_ID= " + std::to_string(userId) + "; ", DatabaseAccess::callback_check_if_got_results, &res, "could not check if album exists");
	return res == 1;
}

Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	for (auto& album : this->_m_albums) {
		if (albumName == album.getName()) {
			return album;
		}
	}
	throw MyException("No album with name " + albumName + " exists");
}

void DatabaseAccess::printAlbums()
{
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const Album& album : this->getAlbums()) 
	{
		std::cout << std::setw(5) << "* " << album;
	}
}

User DatabaseAccess::getUser(int userId) {
	for (const auto& user : this->_m_users) {
		if (user.getId() == userId) {
			return user;
		}
	}

	throw ItemNotFoundException("User", userId);
}

std::list<User> DatabaseAccess::getUsers()
{
	users.clear();

	this->runSqlStatement("SELECT * FROM USERS;", callback_users, nullptr, "Could not get users");
	
	this->_m_users.assign(users.begin(), users.end());

	return users;
}

void DatabaseAccess::printUsers()
{
	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;

	for (const auto& user : this->getUsers()) {
		std::cout << user << std::endl;
	}
}
 
void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture) //
{
	this->runSqlStatement("INSERT INTO PICTURES (NAME, LOCATION, CREATION_DATE, ALBUM_ID) VALUES(\"" + picture.getName() + "\", \"" + picture.getPath() + "\",\"" + picture.getCreationDate() + "\"," + std::to_string(this->getAlbumId(albumName)) + ");", nullptr, nullptr, "could not add picture");
	//adding to TAGS
	for (const int user : picture.getUserTags())
		this->runSqlStatement("INSERT INTO TAGS (PICTURE_ID, USER_ID) VALUES(" + std::to_string(picture.getId()) + ", " + std::to_string(user) + ");", nullptr, nullptr, "could not add a tag");

	auto result = getAlbumIfExists(albumName);

	(*result).addPicture(picture);
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)//
{
	auto result = getAlbumIfExists(albumName);

	this->runSqlStatement("DELETE FROM TAGS WHERE PICTURE_ID=" + std::to_string((*result).getPicture(pictureName).getId()) + ";", nullptr, nullptr, "could not remove tag of picture");
	this->runSqlStatement("DELETE FROM PICTURES WHERE ALBUM_ID=" + std::to_string(this->getAlbumId(albumName)) + " AND NAME=\"" + pictureName + "\";", nullptr, nullptr, "could not remove picture");

	(*result).removePicture(pictureName);
}

int DatabaseAccess::callback_check_if_got_results(void* data, int argc, char** argv, char** azColName)
{
	if (argc > 0) *(int*)data = 1;
	else *(int*)data = 0;
	return 0;
}

int  DatabaseAccess::callback_users(void* data, int argc, char** argv, char** azColName)
{
	users.push_back(User(std::atoi(argv[0]), std::string(argv[1])));
	return 0;
}

int DatabaseAccess::callback_get_ids_or_count(void* data, int argc, char** argv, char** azColName)
{
	ids.push_back(std::atoi(argv[0]));

	if(data != nullptr) *(std::vector<int>*)data = ids;

	return 0;
}

User DatabaseAccess::getTopTaggedUser()
{
	std::vector<int> topTaggedUser;
	ids.clear();
	this->runSqlStatement("SELECT USER_ID FROM TAGS GROUP BY USER_ID HAVING COUNT(USER_ID) = (SELECT MAX(CNT) FROM(SELECT USER_ID, COUNT(USER_ID) CNT FROM TAGS GROUP BY USER_ID));", DatabaseAccess::callback_get_ids_or_count, &topTaggedUser, "Failed to find most tagged user");

	if (!topTaggedUser.empty())return getUser(topTaggedUser[0]);
	else throw MyException("There isn't any tagged user.");
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	std::vector<int> mostTaggedPic;
	ids.clear();

	this->runSqlStatement("SELECT PICTURE_ID FROM TAGS GROUP BY PICTURE_ID HAVING COUNT(PICTURE_ID) = (SELECT MAX(CNT) FROM(SELECT PICTURE_ID, COUNT(PICTURE_ID) CNT FROM TAGS GROUP BY PICTURE_ID));", DatabaseAccess::callback_get_ids_or_count, &mostTaggedPic, "Failed to find most tagged picture");

	if (!mostTaggedPic.empty())return getPicture(mostTaggedPic[0]);
	else throw MyException("There isn't any tagged picture.");
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> pictures;

	std::vector<int> id_pictures;
	ids.clear();

	this->runSqlStatement("SELECT PICTURE_ID FROM TAGS WHERE USER_ID=" + std::to_string(user.getId()) + ';', DatabaseAccess::callback_get_ids_or_count, &id_pictures, "Failed to find tagged pictures of user");

	for (int i = 0; i < id_pictures.size(); i++)
	{
		pictures.push_back(getPicture(id_pictures[i]));
	}
	
	if (!pictures.empty()) return pictures;
	else throw MyException("There isn't any tagged picture.");
}

int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	std::vector<int> albumsCount;
	ids.clear();

	this->runSqlStatement("SELECT COUNT(ID) AS NUMBER_OF_ALBUMS FROM ALBUMS WHERE USER_ID=" + std::to_string(user.getId()) + ';', DatabaseAccess::callback_get_ids_or_count, &albumsCount, "Failed to find number of albums");

	if (!albumsCount.empty()) return albumsCount[0];
	else return 0;
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	std::vector<int> tagsCount;
	ids.clear();

	this->runSqlStatement("SELECT COUNT(ID) AS COUNT_TAGGED FROM TAGS WHERE USER_ID= " + std::to_string(user.getId()) + ';', DatabaseAccess::callback_get_ids_or_count, &tagsCount, "Failed to find number of tags");
	if (!tagsCount.empty()) return tagsCount[0];
	else return 0;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int albumsTaggedCount = countAlbumsTaggedOfUser(user);

	if (0 == albumsTaggedCount) {
		return 0;
	}

	return static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	int albumsCount = 0;

	for (const auto& album : this->_m_albums) {
		const std::list<Picture>& pics = album.getPictures();

		for (const auto& picture : pics) {
			if (picture.isUserTagged(user)) {
				albumsCount++;
				break;
			}
		}
	}

	return albumsCount;
}

int DatabaseAccess::getNextPictureId()
{
	ids.clear();
	this->runSqlStatement("SELECT ID FROM PICTURES;", callback_get_ids_or_count, nullptr, "could not get next picture id");
	if (!ids.empty()) return ids.back();
	else return 0;
}

int DatabaseAccess::getNextUserId()
{
	ids.clear();
	this->runSqlStatement("SELECT ID FROM USERS;", callback_get_ids_or_count, nullptr, "could not get next user id");
	if (!ids.empty()) return ids.back();
	else return 0;
}