#pragma once

#include <iostream>
#include <vector>
#include <io.h>
#include "sqlite3.h"
#include "IDataAccess.h"
#include "Album.h"
#include "MyException.h"
#include "ItemNotFoundException.h"

#define NAME "galleryDB.sqlite"
#define TABLES_CNT 4

class DatabaseAccess : public IDataAccess
{
public:
	DatabaseAccess();
	virtual ~DatabaseAccess();
	// album related
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	void deleteAlbum(const std::string& albumName, int userId) override;
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void closeAlbum(Album& pAlbum) override;
	void printAlbums() override;

	// picture related
	void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;

	// user related
	void printUsers() override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override;
	bool doesUserExists(int userId) override;
	User getUser(int userId) override;

	// user statistics
	int countAlbumsOwnedOfUser(const User& user) override;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override;

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;

	bool open() override;
	void close() override;
	void clear() override;

	int getNextPictureId();
	int getNextUserId();

private:
	std::list<Album> _m_albums;
	std::list<User> _m_users;
	sqlite3* _db;

	void runSqlStatement(std::string sql_statement, int (*callback)(void* data, int argc, char** argv, char** azColName), void* data_output, std::string errOutput);
	void cleanUserData(const User& user);

	auto getAlbumIfExists(const std::string& albumName);
	auto getPicture(const int pictureId);

	std::list<User> getUsers();

	int getAlbumId(const std::string& albumName);

	void setDataOfAlbums(std::list<Album>& albums);

	//callbacks
	static int callback_get_ids_or_count(void* data, int argc, char** argv, char** azColName);
	static int callback_albums(void* data, int argc, char** argv, char** azColName);
	static int callback_pictures(void* data, int argc, char** argv, char** azColName);
	static int callback_users(void* data, int argc, char** argv, char** azColName);
	static int callback_check_if_got_results(void* data, int argc, char** argv, char** azColName);
};